import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="pass",
    database="CPRDB"
    )

mycurser = db.cursor()

mycurser.execute("CREATE TABLE CPR (CPR_ID INT PRIMARY KEY AUTO_INCREMENT NOT NULL, CPR_NUM INT UNSIGNED)")
mycurser.execute("DESCRIBE CPR")

for x in mycurser:
    print(x)

#!C:\Users\Emil-PC\AppData\Local\Programs\Python\Python311\python.exe
print("Content-Type: text/html\n\n")
import cv2 as cv
import numpy as np
import pyzbar.pyzbar as pyzbar
import urllib.request


print("Connecting to Cam")

# Webserver url where the ESP32Cam posts images
url = 'http://192.168.137.221/'

#cv2.namedWindow("ESP32CAM", cv2.WINDOW_AUTOSIZE)

cpr = ""
prev = ""
pres = ""
em = ""

while True:
        # Opens the url containing a jpg image
        img_cap = urllib.request.urlopen(url + 'cam-mid.jpg',)
        # Converting image to a bytearray using unit8 as the container
        # Using unit8 as the container which is a scalar for the BGR color data
        imgnp = np.array(bytearray(img_cap.read()), dtype=np.uint8)
        # Reading the image data
        img = cv.imdecode(imgnp, cv.IMREAD_GRAYSCALE)
        #img = cv.adaptiveThreshold(img, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, \
        #                           cv.THRESH_BINARY, 11, 5)

        # Looking for elements in the image that can be decoded by Pyzbar
        decodedObjects = pyzbar.decode(img)
        for obj in decodedObjects:
            pres = obj.data
            # If no barcode is found, pass
            if prev == pres:
                pass
            # If a barcode is detected then its going to be stored in the variable "cpr"
            else:
                cpr = str(obj.data)[2:12]
                print("CPR:" + cpr)
                prev = pres

        cv.imshow("ESP32CAM", img)
        print(pres)



        key = cv.waitKey(10)
cv.destroyAllWindows()


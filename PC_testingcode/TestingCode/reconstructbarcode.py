#!C:\Users\Emil-PC\AppData\Local\Programs\Python\Python311\python.exe
print("Content-Type: text/html\n\n")
import cv2 as cv
import numpy as np
import pyzbar.pyzbar as pyzbar
import urllib.request


print("Connecting to Cam")

# Webserver url where the ESP32Cam posts images
url = 'http://192.168.137.210/'

#cv2.namedWindow("ESP32CAM", cv2.WINDOW_AUTOSIZE)

cpr = ""
prev = ""
pres = ""
em = ""

while True:
    img_cap = urllib.request.urlopen(url + 'cam-mid.jpg', )
    imgnp = np.array(bytearray(img_cap.read()), dtype=np.uint8)
    img = cv.imdecode(imgnp, cv.IMREAD_GRAYSCALE)
    closed = cv.morphologyEx(img, cv.MORPH_CLOSE, cv.getStructuringElement(cv.MORPH_RECT, (1, 21)))
    print(img.shape)
    dens = np.sum(img, axis=0)
    mean = np.mean(dens)
    thresh = closed.copy()
    for idx, val in enumerate(dens):
        if val < 10800:
            thresh[:, idx] = 0

    (_, thresh2) = cv.threshold(thresh, 255, 500, cv.THRESH_BINARY + cv.THRESH_OTSU)

    cv.imshow("ESP32CAM", thresh2)
    key = cv.waitKey(1)
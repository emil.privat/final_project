const char BUTTON_PIN = 2;
bool pressed = false;

void setup() {
  Serial.begin(115200);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.println("testing");

}

void loop() {
  bool currentState = digitalRead(BUTTON_PIN);

  if (currentState == pressed) {
    Serial.println("pressed");
    delay(1000);
  }
}

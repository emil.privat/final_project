#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <HTTPClient.h>

const char* ssid = "ESPInet";
const char* password = "gtu112112";
const char* htmlMessage = "<p>Button:OFF</p>";
const char BUTTON_PIN = 2;
bool pressed = false;

AsyncWebServer server(80);

void setup() {
  Serial.begin(115200);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.println("testing");


  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi....");
    Serial.println(".................................");
    
  }

  Serial.println(WiFi.localIP());

    server.on("/html", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", htmlMessage);
  });

  server.begin();
}


void loop() {
  
    bool currentState = digitalRead(BUTTON_PIN);
  
    if (currentState == pressed) {
      htmlMessage = "<p>Button:ON</p>";
      Serial.println("Button set to ON");
      
    }
     else {
      htmlMessage = "<p>Button:OFF</p>";
      Serial.println("Button set to OFF");
     }
    
    if ((WiFi.status() == WL_CONNECTED)) {
      HTTPClient http;
  
      http.begin("http://192.168.1.100/tags.html");
      int httpcode = http.GET();
  
      if (httpcode > 0) {
  
        String payload = http.getString();
        Serial.println(httpcode);
        Serial.println(payload);
      }
  
      else {
        Serial.println("Error on http request");
      }
  
      http.end();
      
    }
  
    delay(1000);
}

#include <WiFi.h>
#include <HTTPClient.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_camera.h"

const char RED_LED1 = 14;
const char RED_LED2 = 15;
const char GREEN_LED = 13;

const char BUTTON_PIN = 2; // initiating the buttons pin
bool pressed = false; // false is press and true is not pressed

const char* ssid = "ESPInet";
const char* password = "gtu112112";

String output26State = "off";
String output27State = "off";

const int output26 = 26;
const int output27 = 27;

IPAddress local_IP(192, 168, 2, 10); // Seting Static IP address
IPAddress gateway(192, 168, 2, 1); // Set setting Gateway IP address
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8); 
IPAddress secondaryDNS(8, 8, 4, 4); 

String serverName = "192.168.1.100";   // Webserver address
const int serverPort = 80; //Webserver Port
String serverPath = "/upload.php";     // Path to the php script that handles post request

WiFiClient client;

// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

void setup() {
  
  Serial.begin(115200);

  pinMode(BUTTON_PIN, INPUT_PULLUP); //setting the button pin to and input with the pull up activated
  pinMode(RED_LED1, OUTPUT);
  pinMode(RED_LED2, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

// Testing LEDs
  digitalWrite(RED_LED1, HIGH);
  delay(100);
  digitalWrite(RED_LED1, LOW);
  delay(100);
  digitalWrite(RED_LED2, HIGH);
  delay(100);
  digitalWrite(RED_LED2, LOW);
  delay(100);
  digitalWrite(GREEN_LED, HIGH);
  delay(100);
  digitalWrite(GREEN_LED, LOW);
  
  
  if(!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
  Serial.println("STA Failed to configure");
  }
  
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
    }
    Serial.println("connected to WiFi");
    Serial.println();
    Serial.print("ESP32-CAM IP Address: ");
    Serial.println(WiFi.localIP());
    
    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sscb_sda = SIOD_GPIO_NUM;
    config.pin_sscb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_JPEG;
    config.grab_mode = CAMERA_GRAB_LATEST;
  
    // init with high specs to pre-allocate larger buffers
    if(psramFound()){
      config.frame_size = FRAMESIZE_SVGA;
      config.jpeg_quality = 10;  //0-63 lower number means higher quality
      config.fb_count = 2;
    } else {
      config.frame_size = FRAMESIZE_CIF;
      config.jpeg_quality = 10;  //0-63 lower number means higher quality
      config.fb_count = 1;
    }
    
    // camera init
    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
      Serial.printf("Camera init failed with error 0x%x", err);
      delay(1000);
      Serial.println("Restart in camara init"); 
      ESP.restart();
    }
    
    Serial.println("End of startup"); 
}

void loop() {
  
  Serial.print("#");
  digitalWrite(RED_LED1, HIGH);
  delay(250);
  digitalWrite(RED_LED1, LOW);
  delay(250);
  
  bool currentState = digitalRead(BUTTON_PIN);

  if (currentState == pressed) {
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED2, LOW);
    Serial.println("Button pressed");
    delay(500);
    sendPhoto();
    Serial.println("end of send photo function");
    delay(2800);
    cprStatusCheck();
  }   
}

String sendPhoto() {
  Serial.println("Staring sendPhoto function");
  String getAll;
  String getBody;

  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if(!fb) {
    Serial.println("Camera capture failed");
    Serial.println("camara_fb_t restart");
    ESP.restart();
  }
  
  Serial.println("Connecting to server: " + serverName);

  if (client.connect(serverName.c_str(), serverPort)) {
    Serial.println("Connection successful!");    
    String head = "--EmilFinalProject\r\nContent-Disposition: form-data; name=\"imageFile\"; filename=\"esp32-cam.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
    Serial.println("after head string");
    String tail = "\r\n--EmilFinalProject--\r\n";
    Serial.println("after tail string");
    uint32_t imageLen = fb->len;
    uint32_t extraLen = head.length() + tail.length();
    uint32_t totalLen = imageLen + extraLen;

    
    client.println("POST " + serverPath + " HTTP/1.1");
    client.println("Host: " + serverName);
    client.println("Content-Length: " + String(totalLen));
    client.println("Content-Type: multipart/form-data; boundary=EmilFinalProject");
    client.println();
    client.print(head);


    Serial.println("starting to write to client");
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n=0; n<fbLen; n=n+1024) {
      digitalWrite(RED_LED1, HIGH);
      Serial.print(".");
      if (n+1024 < fbLen) {
        client.write(fbBuf, 1024);
        fbBuf += 1024;
        digitalWrite(RED_LED1, LOW);
      }
      else if (fbLen%1024>0) {
        size_t remainder = fbLen%1024;
        client.write(fbBuf, remainder);
        Serial.print("-");
      }
    }   
    client.print(tail);
    Serial.println("finished writing to client");
    Serial.println("");

    esp_camera_fb_return(fb);
   
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;

    Serial.println("timer");
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      digitalWrite(RED_LED1, HIGH);
      delay(50);      
      digitalWrite(RED_LED1, LOW);
      delay(50);
      while (client.available()) {
        char c = client.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { 
          getBody += String(c); 
          }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    client.stop();
    Serial.println(getBody);
  }
  else {
    getBody = "Connection to " + serverName +  " failed.";
    Serial.println(getBody);
 
  }
  return getBody;
 }

 String cprStatusCheck(){
  String cprStatus;
    HTTPClient http;

    http.begin("http://192.168.1.100/tags.html");
    int httpcode = http.GET();

    if (httpcode > 0) {

      String payload = http.getString();
      Serial.println(httpcode);
      cprStatus = payload.substring(44, 45);
      Serial.println(cprStatus);
    }

    else {
      Serial.println("Error on http request");
    }

    http.end();
    int check = cprStatus.toInt();
    if (check == 1) {
      digitalWrite(GREEN_LED, HIGH);
      Serial.println("in db");
      }
      else if (check == 0) {
        Serial.println("not in db");
        digitalWrite(RED_LED2, HIGH);
      }
      return cprStatus;
  }
  
 

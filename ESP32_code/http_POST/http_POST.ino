#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <HTTPClient.h>
#include <esp32cam.h>

const char* ssid = "ESPInet";
const char* password = "gtu112112";

String output26State = "off";
String output27State = "off";

const int output26 = 26;
const int output27 = 27;

IPAddress local_IP(192, 168, 2, 10); // Seting Static IP address
IPAddress gateway(192, 168, 2, 1); // Set setting Gateway IP address
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8); 
IPAddress secondaryDNS(8, 8, 4, 4); 


void setup() {
  
  Serial.begin(115200);

  WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi....");
    Serial.println(".................................");
    }

    Serial.println("connected to WiFi");
 
}

void loop() {
  auto frame = esp32cam::capture();
  if ((WiFi.status() == WL_CONNECTED)) {

    HTTPClient http;

    Serial.println("http begin");
    http.begin("http://192.168.1.100/get.php");
    http.addHeader("Content-Type", "text/plain"); //  image/jpegtest/plain

    int httpResponseCode = http.POST("this is a test");
  
    if(httpResponseCode>0){
  
      String response = http.getString();
  
      Serial.println(httpResponseCode);
      Serial.println(response);
    }
      else{
        Serial.println("Error posting"); 
      }
  
      http.end();
  }

  else {
    Serial.println("WiFi failed to connect");
  }
  
  delay(10000);
  
}

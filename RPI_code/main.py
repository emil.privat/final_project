import cv2 as cv
import numpy as np
import pyzbar.pyzbar as pyzbar
import mysql.connector
import time

db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="CPRDB"
)

query = "SELECT CPR_NUM FROM CPR WHERE CPR_NUM = %s"
# Webserver tags file
tags = "tags.html"
# Connecting to webserver
CPR_status = 0

def update_CPRstatus(old, new):
    old = str(old)
    new = str(new)
    f_handler = open(tags)
    f_read = f_handler.read()
    tag1 = str("<p>CPRNUMBER_status:"+old+"</p>")
    tag2 = str("<p>CPRNUMBER_status:"+new+"</p>")
    newtags = f_read.replace(tag1, tag2)
    time.sleep(1)
    f_handler = open(tags, "w")
    f_handler.write(newtags)
    f_handler.close()

def update_UploadStatus(old, new):
    old = str(old)
    new = str(new)
    f_handler = open(tags)
    f_read = f_handler.read()
    tag1 = str("<p>Upload_status:"+old+"</p>")
    tag2 = str("<p>Upload_status:"+new+"</p>")
    newtags = f_read.replace(tag1, tag2)
    time.sleep(1)
    f_handler = open(tags, "w")
    f_handler.write(newtags)
    f_handler.close()


test = 0

while True:
    time.sleep(1)
    f_handler = open(tags)
    f_read = f_handler.read()
    f_handler.close()
    lines = f_read.splitlines()
    if lines[0] == "<p>Upload_status:1</p>":
        print("starting main loop")
        update_UploadStatus(1, 0)
    else:
        print("no upload yet")
        continue
    if CPR_status == 1:
        update_CPRstatus(1, 0)
        print("Updating CPR satus in tags.html to 0")
    img = img = cv.imread("./posts/post-esp32-cam.jpg", cv.IMREAD_COLOR)
    barcodes = pyzbar.decode(img)
    barcodeData = 0000000000
    try:
        for barcode in barcodes:
            (x, y, w, h) = barcode.rect
            cv.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type
            text = "{} ({})".format(barcodeData, barcodeType)
            cv.putText(img, text, (x, y - 10), cv.FONT_HERSHEY_SIMPLEX,
                0.5, (0, 0, 255), 2)

            print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))
    except:
        print("No barcode data")
    print(barcodeData)
    CPR = barcodeData
    print("CPR Number: ", CPR)
    mycurser = db.cursor()
    print("Seaching Database")
    mycurser.execute(query,(CPR,))
    data = mycurser.fetchone()
    try:
        if data[0] == int(CPR):
            print("In DB")
            CPR_status = 1
            update_CPRstatus(0, 1)
            print("Updating CPR satus in tags.html to 1")
    except:
        print("Not In DB")
        CPR_status = 0
        update_CPRstatus(1, 0)
        print("Updating CPR satus in tags.html to 0")
    time.sleep(1) 
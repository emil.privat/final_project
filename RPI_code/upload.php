<?php
$tags = "tags.html";
$myfile = fopen($tags, "r");
$myfile_r = fread($myfile,filesize("tags.html"));
$target_dir = "posts/";
$newname = "post-";
$target_file = $target_dir . $newname . basename($_FILES["imageFile"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
  if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  }
  else {
    echo "File is not an image.";
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  unlink($target_fil);  
  echo "testing.";
  $uploadOk = 1;
}

// Check file size
if ($_FILES["imageFile"]["size"] > 500000) {
  echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
}
else {
  if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
    echo "The file ". basename( $_FILES["imageFile"]["name"]). " has been uploaded.";
    $new_file = str_replace("Upload_status:0", "Upload_status:1", $myfile_r);
    // Changing upload status in tags file
    $myfile = fopen($tags, "w");
    fwrite($myfile, $new_file);
    fclose($myfile);
  }
  else {
    echo "Sorry, there was an error uploading your file.";
  }
}
?>